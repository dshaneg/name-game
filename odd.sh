#! /bin/bash

. colors.sh

clear

while [[ true ]]
do
  read -p "Enter a number ('q' to quit). ${green}" number
  printf ${reset}
  if [[ $number == 'q' ]]; then
    cowsay "later dude"
    exit
  fi

  if [ $(( $number % 2 )) -eq 0 ]; then
    status="${red}even${reset}"
  else
    status="${yellow}odd${reset}"
  fi

  echo "${green}${number}${reset} is ${status}."
  echo
done

