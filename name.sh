#! /bin/bash

. ./colors.sh

again=yes

while [[ ${again} =~ ^[Yy].* ]]
do
  clear

  read -p "What is your name? ${green}" name

  name=${red}${name}${reset}

  read -p $"${reset}Hello ${name}! I'm the computer. How are you today? ${green}" status

  printf "${reset}\nCool, I'm feeling ${status} today too, ${name}.\n\n"

  read -p "Wanna play the name game again? ${green}" again

  printf ${reset}
done

cowsay "Bye ${name}!"
